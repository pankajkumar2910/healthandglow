package com.healthglow.client;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pankaj on 26/9/16.
 */

public class FetchDataTask extends AsyncTask<Void,Void,String> {

    String TAG = FetchDataTask.class.getSimpleName();

    FetchDataCallBack callBack;

    public FetchDataTask(FetchDataCallBack dataCallBack){
        callBack = dataCallBack;
    }

    @Override
    protected String doInBackground(Void... voids) {

        try {
            return sendPostRequest();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result != null){
            try {
                List<SkuItem> skuItemList = processJson(result);
                callBack.onSkuDataFetch(skuItemList);
                for (SkuItem item :skuItemList) {
                    Log.d(TAG,"----------------------------");
                    Log.v(TAG,"Name : " + item.getName());
                    Log.v(TAG,"Offer desc: " + item.getOfferDesc());
                    Log.v(TAG,"Offer Price: " + item.getOfferPrice());
                    Log.v(TAG,"Original Price : " + item.getPrice());
                    Log.v(TAG,"Total Price: " + item.getTotalPrice());
                    Log.v(TAG,"Product Info: " + item.getProductInformation());
                    Log.v(TAG,"Img url: " + item.getImageUrl());
                    Log.d(TAG,"----------------------------");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private List<SkuItem> processJson(String jsonResponse) throws JSONException{
        JSONObject rootObject = new JSONObject(jsonResponse);
        JSONObject dataObj = rootObject.getJSONObject("data");
        JSONArray skuItemsArray =  dataObj.getJSONArray("skuItems");
        List<SkuItem> skuItemList = new ArrayList<>(skuItemsArray.length());
        for (int i = 0; i < skuItemsArray.length(); i++) {
            SkuItem skuItem = new SkuItem();
            JSONObject skuItemObj = skuItemsArray.getJSONObject(i);
            skuItem.setName(skuItemObj.getString("skuName"));
            skuItem.setPrice(skuItemObj.getDouble("skuPrice"));
            skuItem.setOfferPrice(skuItemObj.getDouble("skuOfferPrice"));
            if(skuItemObj.get("skuTotalPrice").equals("null")){
                skuItem.setTotalPrice(0);
            }
            skuItem.setOfferDesc(skuItemObj.getString("skuOfferDesc"));
            skuItem.setProductInformation(skuItemObj.getString("skuProductInformation"));
            skuItem.setImageUrl(skuItemObj.getJSONArray("skuImageUrls").getString(0));
            skuItemList.add(skuItem);
        }

        return skuItemList;
    }

    private String sendPostRequest() throws IOException,JSONException{
        String url="http://119.81.82.197:9090/hngeCommerceWebservice/rest/product/categoryNew/";
        URL object=new URL(url);

        HttpURLConnection con = (HttpURLConnection) object.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("POST");

        JSONObject rootObject   = new JSONObject();

        rootObject.put("appType","ANDROID");
        rootObject.put("appVersion", "2.0.0.1");
        rootObject.put("batchSize", "20");
        rootObject.put("categoryId", "NAILPOLISH");

        JSONObject filterInnerObj = new JSONObject();
        filterInnerObj.put("selectedBrand",new JSONArray());
        filterInnerObj.put("selectedCategory",new JSONArray());
        filterInnerObj.put("selectedPrice",new JSONArray());

        rootObject.put("filter",filterInnerObj);
        rootObject.put("lastItemCount",0);
        rootObject.put("pinNumber",560103);

        JSONObject sortObj = new JSONObject();
        sortObj.put("sortBy","");
        sortObj.put("sortOrder","");

        rootObject.put("sort",sortObj);

        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(rootObject.toString());
        wr.flush();

        StringBuilder sb = new StringBuilder();
        int HttpResult = con.getResponseCode();
        if (HttpResult == HttpURLConnection.HTTP_OK) {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        } else {
            System.out.println(con.getResponseMessage());
            return null;
        }
    }
}

package com.healthglow.client;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Random;

/**
 * Created by pankaj on 27/9/16.
 */

public class MySkuAdapter extends RecyclerView.Adapter<MySkuAdapter.SkuViewHolder> {

    private static final String TAG = MySkuAdapter.class.getSimpleName();
    private List<SkuItem> mSkuDataList;
    Context mContext;

    public MySkuAdapter(List<SkuItem> myDataset, Context mContext) {
        mSkuDataList = myDataset;
        this.mContext = mContext;
    }

    @Override
    public SkuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        SkuViewHolder vh = new SkuViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(SkuViewHolder holder, int position) {
        SkuItem mItem = mSkuDataList.get(position);
        String imageUrl = mItem.getImageUrl();
        String skuDesc = mItem.getName().toLowerCase();
        double skuFinalPrice = mItem.getPrice();
        double skuOfferPrice = mItem.getOfferPrice();
        double skuTotalPrice = mItem.getTotalPrice();

        skuDesc = skuDesc.substring(0,1).toUpperCase() + skuDesc.substring(1);

        holder.skuDesc.setText(skuDesc);
        holder.skuFinalPrice.setText("Rs "+ String.valueOf(skuFinalPrice));

        String offerPrice = "Rs "+String.valueOf(skuOfferPrice);
        StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
        holder.skuOriginalPrice.setText(offerPrice, TextView.BufferType.SPANNABLE);
        Spannable spannable = (Spannable) holder.skuOriginalPrice.getText();
        spannable.setSpan(STRIKE_THROUGH_SPAN, 0, offerPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Glide.with(mContext)
                .load(SkuUtility.getRandomImage().get(randInt(0,7)))
                .error(R.mipmap.ic_error)
                .crossFade()
                .into(holder.skuImage);

    }

    public static int randInt(int min, int max) {

        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }



    @Override
    public int getItemCount() {
        return mSkuDataList.size();
    }

    public class SkuViewHolder extends RecyclerView.ViewHolder{

        TextView skuDesc,skuOriginalPrice,skuFinalPrice,skuOffer;
        ImageView skuImage;
        public SkuViewHolder(View itemView) {
            super(itemView);
            skuDesc = (TextView) itemView.findViewById(R.id.skuDescription);
            skuOriginalPrice = (TextView) itemView.findViewById(R.id.originalPriceTV);
            skuFinalPrice = (TextView) itemView.findViewById(R.id.finalPriceTV);
            skuOffer = (TextView) itemView.findViewById(R.id.skuOffer);
            skuImage = (ImageView) itemView.findViewById(R.id.skuImage);
        }
    }

}

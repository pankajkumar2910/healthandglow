package com.healthglow.client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pankaj on 28/9/16.
 */

final class SkuUtility {
    public static List<String> getRandomImage(){
        String url1 = "http://i.huffpost.com/gen/1326029/thumbs/h-OVERHYPED-BEAUTY-PRODUCTS-320x320.jpg";
        String url2 = "https://s-media-cache-ak0.pinimg.com/236x/b3/b1/e9/b3b1e964cc0a884eb6941bdf92665348.jpg";
        String url3 = "https://s-media-cache-ak0.pinimg.com/236x/b3/b1/e9/b3b1e964cc0a884eb6941bdf92665348.jpg";
        String url4 = "https://s-media-cache-ak0.pinimg.com/236x/2d/d1/3a/2dd13a6bb1b2eedb25255e27eb33028b.jpg";
        String url5 = "http://media.allure.com/photos/577190103b5256713da495e4/master/pass/beauty-products-skin-2011-eucerin-dry-skin-therapy-plus-lotion.jpg";
        String url6 = "https://s-media-cache-ak0.pinimg.com/236x/b3/b1/e9/b3b1e964cc0a884eb6941bdf92665348.jpg";
        String url7 = "https://s-media-cache-ak0.pinimg.com/236x/30/71/e2/3071e24355f4499b644f1775e2e4ac2c.jpg";
        String url8 = "http://sev.h-cdn.co/assets/cm/15/08/54e80d9c1cf4b_-_sev-winter-beauty-must-haves-body-scrub-lgn.jpg";
        List<String> imgList = new ArrayList<>();
        imgList.add(url1);
        imgList.add(url2);
        imgList.add(url3);
        imgList.add(url4);
        imgList.add(url5);
        imgList.add(url6);
        imgList.add(url7);
        imgList.add(url8);
        return imgList;
    }

}

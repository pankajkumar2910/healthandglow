package com.healthglow.client;

import java.util.List;

/**
 * Created by pankaj on 27/9/16.
 */

public interface FetchDataCallBack {

    void onSkuDataFetch(List<SkuItem> skuItems);
}

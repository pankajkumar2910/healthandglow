package main;

import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		
		int arrA[] = new int[]{1,2,3,4,7,8,9,11,2,3,1,5,6};
		printProblem(arrA);
	}
	
	private static void printProblem(int arr[]){
		LinkedList<Integer> integerList = new LinkedList<>();
		System.out.println("First Pair :: ");
		for (int i = 0; i < arr.length; i++) {
			int storedIndex = integerList.indexOf(arr[i]);
			if(storedIndex >= 0){
				System.out.println(arr[i] + "," + arr[i]);
				integerList.remove(storedIndex);
			}else{
				integerList.add(arr[i]);
			}
		}
		
		System.out.println("Second Pair :: ");
		
		for (int i = 0; i < integerList.size(); i++) {
			for (int j = i+1; j < integerList.size(); j++) {
				int firstInt = integerList.get(i);
				int secondInt = integerList.get(j);
				if((Math.abs(firstInt - secondInt)) <= 2){
					System.out.println(firstInt + "," + secondInt);
					Integer integer1 = new Integer(firstInt);
					integerList.remove(integer1);
					integerList.remove(new Integer(secondInt));
				}
			}
		}
		System.out.print("Remaining item : ");
		for (int i = 0; i < integerList.size(); i++) {
			System.out.print(integerList.get(i) + ",");
		}
		
	}
	
}
